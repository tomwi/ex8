<?php

use yii\db\Migration;

class m160612_181415_init_client_table extends Migration
{
    public function up()
    {
        $this->createTable (
        'client',
        [
            'id' => 'pk',
            'name' => 'string',
            'project_Name' => 'string',
            'details' => 'string',

        ],
        'ENGINE=InnoDB'
        );
    }
    public function down()
    {
       $this->dropTable('client') ;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
