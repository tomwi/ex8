<?php
namespace app\commands;

use Yii;
use yii\console\Controller;


class UserupdateController extends Controller
{
	public function actionAddrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnUser = $auth->getPermission('updateOwnUser');
		$auth->remove($updateOwnUser);
		
		$rule = new \app\rbac\OwnUserRule;
		$auth->add($rule);
				
		$updateOwnUser->ruleName = $rule->name;		
		$auth->add($updateOwnUser);	
	}




	public function actionAddleadrule()
	{
		$auth = Yii::$app->authManager;				
		$updateOwnLead = $auth->getPermission('updateOwnLead');
		$auth->remove($updateOwnLead);
		
		$rule = new \app\rbac\OwnLeadRule;
		$auth->add($rule);
				
		$updateOwnLead->ruleName = $rule->name;		
		$auth->add($updateOwnLead);	
	}

	
}