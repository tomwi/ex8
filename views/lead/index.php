<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use yii\data\ActiveDataPrivider;


/* @var $this yii\web\View */
/* @var $model app\models\Lead */

$this->title = 'Leads';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="lead-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php if(yii::$app->user->can('createLead')){ //hiding the update button from unaoturized users ?> 

    

   <?= Html::a('Create Lead', ['create'], ['class' => 'btn btn-success']) ?> 
          
                <?php }     ?>         

    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],            
            'id',
            'name',
            'email:email',
            'phone',
            'notes:ntext',

          //       'status',

  [
       'attribute' => 'status',
                'label' => 'Status',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->status, 
                    ['user/view', 'id' => $model->id]);
                },
                'filter'=>Html::dropDownList('LeadSearch[status]', $status, $statuses, ['class'=>'form-control']),
                   ], 
          [
       'attribute' => 'owner',
                'label' => 'Owner',
                'format' => 'raw',
                'value' => function($model){
                    return Html::a($model->userOwner->fullname, 
                    ['user/view', 'id' => $model->userOwner->id]);
                },
                'filter'=>Html::dropDownList('LeadSearch[owner]', $owner, $owners, ['class'=>'form-control']),
            //    'filter'=>Html::dropDownList('LeadSearch[status]', $owner, $owners, ['class'=>'form-control']),

            ], 

            // 'owner',
            // 'created_at',
            // 'updated_at',
            // 'created_by',
            // 'updated_by',

            ['class' => 'yii\grid\ActionColumn'],
        ],
       // $query = post::find()->where(['status => 1']);

    ]); ?>
</div>