<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;  
use yii\web\IdentityInterface;


/**
 * This is the model class for table "client".
 *
 * @property integer $id
 * @property string $name
 * @property string $project_Name
 * @property string $details
 */
class Client extends ActiveRecord implements \yii\web\IdentityInterface
{
     const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    
    /**
     * @inheritdoc
     */
    public $client;
    public static function tableName()
    {
        return 'client';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'project_Name', 'details'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'project_Name' => 'Project  Name',
            'details' => 'Details',
            'clients' => 'clients',
        ];
    }
   
     public function getFullname()
    {
        return $this->firstname.' '.$this->lastname;
    }
 public static function getUsers()
  {
    $users = ArrayHelper::
          map(self::find()->all(), 'id', 'fullname');
    return $users;            
  }
  public static function findIdentity($id)
    {
        return static::findOne($id);
    }
    public static function findByUsername($username)
  {
    return static::findOne(['username' => $username]);
  }
  public static function findIdentityByAccessToken($token, $type = null)
    {
    throw new NotSupportedException('You can only login
              by username/password pair for now.');
    }
    public function getId()
    {
        return $this->id;
    }
    public function getAuthKey()
    {
        return $this->auth_key;
    }
    public function validateAuthKey($authKey)
    {
         return $this->getAuthKey() === $authKey;
    } 
public function validatePassword($password)
  {
    return $this->isCorrectHash($password, $this->password); 
  }

  private function isCorrectHash($plaintext, $hash)
  {
    return Yii::$app->security->validatePassword($plaintext, $hash);
  }
}
